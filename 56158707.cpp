#include <iostream>
#include <cstring>
#include <readline/readline.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include "56158707_helper.h"

using namespace std;

int last_signal = -1;

// int prun[ARR_LEN], pstp[ARR_LEN], cprun = 0, cpstp = 0;

CmdInfoList runlist, stoplist, termlist;

void printHeader() {
    // Program heading, for decoration
    cout << "===========================" << endl;
    cout << " CS3103 Operating Systems" << endl;
    cout << " Homework 1" << endl;
    cout << " Lazar Galic (56158707)" << endl;
    cout << " lgalic2-c@my.cityu.edu.hk" << endl;
    cout << "===========================" << endl;
}

void getCommand(char* line, char* arg_array[], int &argno) {

    char* current_arg = strtok(line, " ");
    argno = 0;

    while (current_arg != NULL) {
        arg_array[argno] = current_arg;
        ++argno;
        current_arg = strtok(NULL, " ");
    }

    arg_array[argno] = NULL;
}

void listProcesses() {
    if ((runlist.start() != NULL) || (stoplist.start() != NULL)) {
        CmdInfoNode* info = runlist.start();

        while (info != NULL) {
            cout << info->getPid() << ": running ";

            char** cmdline = info->getCmdLine();

            for (int i=0; (i<ARR_LEN) && (cmdline[i] != NULL); ++i) {
                cout << cmdline[i] << " ";
            }

            cout << endl;

            info = info->getNext();
        }

        info = stoplist.start();

        while (info != NULL) {
            cout << info->getPid() << ": stopped ";

            char** cmdline = info->getCmdLine();

            for (int i=0; (i<ARR_LEN) && (cmdline[i] != NULL); ++i) {
                cout << cmdline[i] << " ";
            }

            cout << endl;

            info = info->getNext();
        }
    }
    else {
        cout << "No processes are running or stopped." << endl;
    }
}

void reapProcesses() {
	int status = 0, pid = 0;

	while ((pid = waitpid(-1, &status, WNOHANG | WUNTRACED)) > 0) {
        if (WIFEXITED(status)) { // Process ended normally
            runlist.remove(pid);
            stoplist.remove(pid);
            cout << "Process " << pid << " completed." << endl;
        }
        else if (WIFSIGNALED(status)) { // Process was terminated
            runlist.remove(pid);
            stoplist.remove(pid);
            cout << "Process " << pid << " terminated." << endl;
        }
        else if (WIFSTOPPED(status)) { // Process was stopped
            CmdInfoNode* info = runlist.find(pid);
            if (info != NULL) {
                runlist.remove(pid);
                stoplist.add(info);
            }
            cout << "Process " << pid << " stopped." << endl;
        }
        else { // Process continued
            CmdInfoNode* info = stoplist.find(pid);
            if (info != NULL) {
                stoplist.remove(pid);
                runlist.add(info);
            }
            cout << "Process " << pid << " continued." << endl;
        }
		status = 0;
	}
}

void killAllChildren() {

    sigset_t mask;

    sigemptyset(&mask);

    // Update info on all child processes
    reapProcesses();

    // Terminate all running child processes
    CmdInfoNode* info = runlist.start(); 
    while (info != NULL) {
        kill(info->getPid(), SIGTERM);
        sigsuspend(&mask);
        info = info->getNext();
    }

    // Terminate all stopped child processes
    info = stoplist.start(); 
    while (info != NULL) {
        kill(info->getPid(), SIGCONT); // to get a SIGTERM, child must be active
        kill(info->getPid(), SIGTERM); // SIGKILL wouldn't require a SIGCONT, but it doesn't allow cleanup
        sigsuspend(&mask);
        info = info->getNext();
    }

    // Reap all terminated children
    reapProcesses();
}

// Main purpose of the handler is to leave the 
void signalHandler(int signum) {
    last_signal = signum;
    reapProcesses(); // This should rarely reap processes
    // I left it here to ensure all processes are reaped shortly after receiving a signal.
    // Most processes should be reaped by other invocations of the reaper.
}

int main(int argc, char **argv) {

    // Declaration of variables
    char *line, *cmd, *current_arg, **cmd_array, **arg_array;

    int argno = 0, status = 0;

    bool ended = false;

    pid_t pid;

    sigset_t mask;

    sigemptyset(&mask);
    sigaddset(&mask, SIGINT);
    // When running sigsuspend, if a SIGINT is sent, it will first be received by the parent
    // and then by the child. However, parent will have already left the waiting loop and printed
    // the prompt " sh > ". This way, we'd end up with the "Process X terminated" message written
    // afterwards, messing up the prompt.

    signal(SIGCHLD, signalHandler);
    signal(SIGINT, signalHandler);
    signal(SIGTSTP, signalHandler);

    printHeader();

    // ENDED indicates whether we should keep reading data
    while (!ended) {
        // Reallocating the pointers
        arg_array = new char*[ARR_LEN];
        line = new char[STR_LEN];

        // Printing the prompt
        cout << endl << "sh > ";

        // Getting the command line (its maximum length is 256, similar to e.g. Windows CMD)
        cin.getline(line, STR_LEN);

        // Reading the command
        getCommand(line, arg_array, argno);

        // If no command was entered, I replicated the behavior of most shells that show a new prompt.
        // I also added an info message, as it makes it clear and consistent.
        if (argno <= 0) {
            cout << "No command was entered!" << endl;
            continue;
        }

        // I assigned the command to a pointer CMD so that the code is more clear
        cmd = arg_array[0];

        // I also assigned the sub-command that will actually be executed to CMD_ARRAY, for easier use
        cmd_array = arg_array+1;

        if (strcmp(cmd, CMD_EXIT) == 0) { // command 'exit'
            cout << "Killing all children..." << endl;

            killAllChildren();

            cout << "Shell program ended." << endl;

            ended = true;
        }
        else if (strcmp(cmd, CMD_FGND) == 0) { // command 'fg'
            if (argno <= 1) {
                cout << "No command to execute given to fg." << endl;
                continue;
            }

            pid = fork();

            if (pid > 0) { // If this is the parent process
                runlist.add(new CmdInfoNode(pid, cmd_array));
                last_signal = -1;

                while (last_signal < 0) { //
					sigsuspend(&mask);
                    reapProcesses();
                }
            }
            else if (pid == 0) { // If this is the child process
                // Execute the command
                status = execvp(cmd_array[0], cmd_array);

                // This code should only be reached if command fails
                if (status < 0) {
                    if (errno == ENOENT) {
                        cout << "Executable " << cmd_array[0] << " not found." << endl;
                    }
                    else {
                        cout << "Executing the command failed. Error code: " << errno << endl;
                    }
                }

                // Child processes should not continue executing the main loop, to prevent a fork bomb
                ended = true;
            }
        }
        else if (strcmp(cmd, CMD_BGND) == 0) { // command 'bg'
            if (argno <= 1) {
                cout << "No command to execute given to bg." << endl;
                continue;
            }

            pid = fork();

            if (pid > 0) { // If this is the parent process
                runlist.add(new CmdInfoNode(pid, cmd_array));
            }
            else if (pid == 0) { // If this is the child process
                setpgid(0, 0);

                // Execute the command
                status = execvp(cmd_array[0], cmd_array);

                // This code should only be reached if command fails
                if (status < 0) {
                    if (errno == ENOENT) {
                        cout << "Executable " << cmd_array[0] << " not found." << endl;
                    }
                    else {
                        cout << "Executing the command failed. Error code: " << errno << endl;
                    }
                }

                // Child processes should not continue executing the main loop, to prevent a fork bomb
                ended = true;
            }
            else { // If an error occured in fork()
                cout << "Forking the process failed. Error code: " << errno << endl;
            }
        }
        else if (strcmp(cmd, CMD_LIST) == 0) {
            reapProcesses();
            listProcesses();
        }
        else {
            cout << "Command " << cmd << " not recognized!" << endl;
        }

        reapProcesses();  // This should (mostly) reap processes in the foreground
        // I left it here to ensure all processes are reaped before the prompt.
    }
}
