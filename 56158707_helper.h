#ifndef __56158707_HELPER_H__
#define __56158707_HELPER_H__

// Constant global variables, to make it easier to adjust some key internal parameters of the program

const char CMD_EXIT[] = "exit";
const char CMD_FGND[] = "fg";
const char CMD_BGND[] = "bg";
const char CMD_LIST[] = "list";

const int STR_LEN = 256; // Maximum length of strings (in particular, shell input line)
const int ARR_LEN = 16;  // Maximum length of args array (thus, maximum number of args)
const int PID_LEN = 64;  // Maximum length of PID arrays

// Structures for the linked list

class CmdInfoNode {
private:
    int pid;
    char** cmdline;
    CmdInfoNode* next;
public:
    CmdInfoNode(int pid, char** cmdline);
    int getPid();
    char** getCmdLine();
    CmdInfoNode* getNext();
    CmdInfoNode* find(int pid);
    void add(CmdInfoNode* node);
    void remove(int pid);
};

class CmdInfoList {
private:
    CmdInfoNode* first;
public:
    CmdInfoList();
    CmdInfoNode* start();
    void add(CmdInfoNode* node);
    CmdInfoNode* find(int pid);
    void remove(int pid);
};

#endif