// I made a separate CPP and H file to hold the class definitions of the command info lists
// separate from the main code as it was already getting quite complicated and hard to maintain

#include "56158707_helper.h"
#include <cstddef>
#include <iostream>

using namespace std;

CmdInfoNode::CmdInfoNode(int pid, char** cmdline) {
    this->pid = pid;
    this->cmdline = cmdline;
    this->next = NULL;
}

int CmdInfoNode:: getPid() {
    return this->pid;
}

char** CmdInfoNode::getCmdLine() {
    return this->cmdline;
}

CmdInfoNode* CmdInfoNode::getNext() {
    return this->next;
}

CmdInfoNode* CmdInfoNode::find(int pid) {
    if (this->pid == pid) {
        return this;
    }

    if (this->next != NULL) {
        return this->next->find(pid);
    }

    return NULL;
}

void CmdInfoNode::add(CmdInfoNode* node) {
    if (this->pid == node->pid) {
        return;
    }

    if (this->next == NULL) {
        this->next = node;
        node->next = NULL;
    }
    else {
        this->next->add(node);
    }
}

void CmdInfoNode::remove(int pid) {
    while (this->next != NULL && this->next->pid == pid) {
        this->next = this->next->next;
    }
    
    if (this->next != NULL) {
        this->next->remove(pid);
    }
}

CmdInfoList::CmdInfoList() {
    this->first = new CmdInfoNode(0, NULL);
}

CmdInfoNode* CmdInfoList::start() {
    return this->first->getNext();
}

void CmdInfoList::add(CmdInfoNode* node) {
    this->first->add(node);
}

CmdInfoNode* CmdInfoList::find(int pid) {
    if (this->first->getNext() != NULL) {
        return this->first->getNext()->find(pid);
    }

    return NULL;
}

void CmdInfoList::remove(int pid) {
    this->first->remove(pid);
}
